all: clientTCP.exe serverTCP.exe

clientTCP.exe: clientTCP.o

	gcc -o clientTCP.exe clientTCP.o

clientTCP.o: clientTCP.c

	gcc -c clientTCP.c

serverTCP.exe: serverTCP.o

	gcc -o serverTCP.exe serverTCP.o

serverTCP.o: serverTCP.c

	gcc -c serverTCP.c

.PHONY:	clean

clean:
	-rm -f *.exe *.o
